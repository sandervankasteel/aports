# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>

pkgname=font-iosevka
pkgver=11.1.1
pkgrel=0
pkgdesc="Versatile typeface for code, from code."
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab
	$pkgname-aile
	$pkgname-etoile
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-etoile-$pkgver.zip
	"

builddir="$srcdir"

_install_font_ttc() {
	font="$srcdir/$1.ttc"
	install -Dm644 "$font" -t "$subpkgdir"/usr/share/fonts/TTC
}

package() {
	for pkg in $subpackages; do
		depends="$depends $pkg"
	done
	mkdir -p "$pkgdir"
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	_install_font_ttc "iosevka"
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	_install_font_ttc "iosevka-slab"
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	_install_font_ttc "iosevka-curly"
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	_install_font_ttc "iosevka-curly-slab"
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	_install_font_ttc "iosevka-aile"
}

etoile() {
	pkgdesc="$pkgdesc (Iosevka Etoile)"
	_install_font_ttc "iosevka-etoile"
}

sha512sums="
c99295718e2797ba6e46c60498c7ef438876cfba5141b25516feba60fa84b50e630840379417196e29da77450212ebbf1e9ac57b621aafaaa3352e005b7233a0  super-ttc-iosevka-11.1.1.zip
9a9bad6b09f835f63b611704c13d53e3f5a550257c52eb9fc073f77d59daad712254c9a5ff1487cc1859b767ade4bcbd9380d92e5e5ade7e872f61addbc42ae9  super-ttc-iosevka-slab-11.1.1.zip
bc712beaae7fb4ae7aca78886d6f7d75bbcea0d13886274366693a194be4eb1fef03bd31a3b55fb0d34282164a350357df0c053b4f80cc899791623daed5b77b  super-ttc-iosevka-curly-11.1.1.zip
b2d78b6aa1c1168ca46044d65959c168dd94fc0ba748bb92e7c8ece24185f2cf647c169024cd7a361ee29ae5f7414ebf26abcd175283359b6395454db8260156  super-ttc-iosevka-curly-slab-11.1.1.zip
eecb9b5e18a89de0edb742f5d8d150b1723f1dae9b4d2821487cf81900755e1a6fb3fbedecab8283b29e3d19115e02d9671a559d4b4ad4df7958e9b6f6314384  super-ttc-iosevka-aile-11.1.1.zip
814d9b43424a862101ad6641e1eb78c77692ff6dfdbd337c0cf3b6746dd1d706a8ebbdcc327cf4eecf204a928135559a2d748a8b2beaf44ddf11655b052f3b86  super-ttc-iosevka-etoile-11.1.1.zip
"
